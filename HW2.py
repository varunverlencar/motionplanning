#!/usr/bin/env python
# -*- coding: utf-8 -*-
#HW2 for RBE 595/CS 525 Motion Planning
#code based on the simplemanipulation.py example
import time
import openravepy

#### YOUR IMPORTS GO HERE ####
import heapq
import numpy as np
#### END OF YOUR IMPORTS ####

if not __openravepy_build_doc__:
    from openravepy import *
    from numpy import *

def waitrobot(robot):
    """busy wait for robot completion"""
    while not robot.GetController().IsDone():
        time.sleep(0.01)

def tuckarms(env,robot):
    with env:
        jointnames = ['l_shoulder_lift_joint','l_elbow_flex_joint','l_wrist_flex_joint','r_shoulder_lift_joint','r_elbow_flex_joint','r_wrist_flex_joint']
        robot.SetActiveDOFs([robot.GetJoint(name).GetDOFIndex() for name in jointnames])
        robot.SetActiveDOFValues([1.29023451,-2.32099996,-0.69800004,1.27843491,-2.32100002,-0.69799996]);
        robot.GetController().SetDesired(robot.GetDOFValues());
    waitrobot(robot)

if __name__ == "__main__":

    env = Environment()
    env.SetViewer('qtcoin')
    env.Reset()
    # load a scene from ProjectRoom environment XML file
    env.Load('data/pr2test2.env.xml')
    time.sleep(0.1)

    # 1) get the 1st robot that is inside the loaded scene
    # 2) assign it to the variable named 'robot'
    robot = env.GetRobots()[0]

    # tuck in the PR2's arms for driving
    tuckarms(env,robot);


    with env:
        # the active DOF are translation in X and Y and rotation about the Z axis of the base of the robot.
        robot.SetActiveDOFs([],DOFAffine.X|DOFAffine.Y|DOFAffine.RotationAxis,[0,0,1])

        goalconfig = [2.6,-1.3,-pi/2]
        #### YOUR CODE HERE ####

        goal = (goalconfig[0],goalconfig[1],goalconfig[2])
        openSet = []
        current = None
        heapq.heapify(openSet)
        closedSet = set()
        cameFrom = {}
        collision_nodes = {}
        explored_nodes ={}
        handles = [] # move below
        g = {}
        f = {}
        h = {}
        computed_path = []
        
        # 
        #### Implement the A* algorithm to compute a path for the robot's base starting from the current configuration of the robot and ending at goalconfig. The robot's base DOF have already been set as active. It may be easier to implement this as a function in a separate file and call it here.
        
        # Manhattan distance
        def heuristicM(node,goal):
            dx = abs(node[0] - goal[0])
            dy = abs(node[1] - goal[1])
            dz = min(abs(node[2] - goal[2]), 2*pi - abs(node[2] - goal[2]))
            return 1*(dx + dy + dz)

        # Euclidean distance 
        def heuristicE(node,goal):
            dx = abs(node[0] - goal[0])
            dy = abs(node[1] - goal[1])
            dz = min(abs(node[2] - goal[2]), 2*pi - abs(node[2] - goal[2]))
            return 1*sqrt(dx * dx + dy * dy + dz * dz)

        def retracePath(c):
            n = c
            computed_path.append(n)
            while n in cameFrom:
                n = cameFrom[n]
                computed_path.append(n)
            computed_path.reverse()
                 
        def update_node(neighbor,node):
            g[neighbor] = g[node] + heuristicE(node,neighbor)
            h[neighbor]= heuristicE(node,neighbor) #change M or E for manhattan or euclidean respectively
            cameFrom[neighbor] = node
            # print node
            #delete node from collision and explored
            if neighbor in collision_nodes.keys():
                del collision_nodes[neighbor]
            if neighbor in  explored_nodes.keys():
                            del  explored_nodes[neighbor]

            f[neighbor] = h[neighbor]+ g[neighbor]
         
        def get_neighbors(node):
            neighbors =[]
            xstep , ystep , zstep = 0.2 , 0.1, pi/4
            
            #4-connected            
            neighbors.append((node[0],node[1]+ystep,node[2]))
            neighbors.append((node[0],node[1]-ystep,node[2]))
            neighbors.append((node[0],node[1],node[2]-zstep))
            neighbors.append((node[0],node[1],node[2]+zstep))
            neighbors.append((node[0]-xstep,node[1],node[2]))
            neighbors.append((node[0]+xstep,node[1],node[2]))
            # print neighbors

            # for 8-connected uncomment this along with 4 connected
            # neighbors.append((node[0]+xstep,node[1]+ystep,node[2]))
            # neighbors.append((node[0]+xstep,node[1]-ystep,node[2]))
            # neighbors.append((node[0]+xstep,node[1],node[2]+zstep))
            # neighbors.append((node[0]+xstep,node[1],node[2]-zstep))

            # neighbors.append((-xstep,node[1]+ystep,node[2]))
            # neighbors.append((-xstep,node[1]-ystep,node[2]))
            # neighbors.append((-xstep,node[1],node[2]+zstep))
            # neighbors.append((-xstep,node[1],node[2]-zstep))

            # neighbors.append((node[0]+xstep,node[1]+ystep,node[2]+zstep))
            # neighbors.append((node[0]+xstep,node[1]-ystep,node[2]+zstep))
            # neighbors.append((node[0]+xstep,node[1]-ystep,node[2]-zstep))
            # neighbors.append((node[0]+xstep,node[1]+ystep,node[2]-zstep))

            # neighbors.append((-xstep,node[1]+ystep,node[2]+zstep))
            # neighbors.append((-xstep,node[1]+ystep,node[2]-zstep))
            # neighbors.append((-xstep,node[1]-ystep,node[2]+zstep))
            # neighbors.append((-xstep,node[1]-ystep,node[2]-zstep))

            # neighbors.append((node[0],node[1]+ystep,node[2]+zstep))
            # neighbors.append((node[0],node[1]+ystep,node[2]-zstep))
            # neighbors.append((node[0],node[1]-ystep,node[2]+zstep))
            # neighbors.append((node[0],node[1]-ystep,node[2]-zstep))

            return neighbors

        def colliding (node):
            robot.SetActiveDOFValues((node[0],node[1],node[2]));
            incollision = env.CheckCollision(robot)   #check collision
            if incollision:
                # collision_nodes.append(node)
                # uncomment below and comment above line for real time visualizing
                handles.append(env.plot3(points=array((node[0],node[1],0.05)), 
                                       pointsize=0.05,
                                       colors=array(((1,0,0))),
                                       drawstyle=1))
                return False
            else:
                # explored_nodes.append(node)
                #uncomment below and comment above line for real time visualizing
                handles.append(env.plot3(points=array((node[0],node[1],0.05)), 
                                       pointsize=0.05,
                                       colors=array(((0,0,1))),
                                       drawstyle=1))
                return True
        
        def aStar(start,goalconfig):
            g[start] = 0
            f[start] = heuristicE(start,goalconfig)    #change M or E for manhattan or euclidean respectively

            heapq.heappush(openSet,(f[start],start))

            while len(openSet):
                # pop node with the least f from heap queue 
                f1,current = heapq.heappop(openSet)
                # add node to closedSet list so we don't process it twice
                closedSet.add(current)
                # if ending node, display found path
                if (abs(current[0]- goal[0]) < 0.09)  and (abs(current[1]- goal[1]) < 0.09) and (current[2] == goal[2]) :      # tolerance?
                    print 'goal reached:',current
                    retracePath(current)
                    # print 'path: ',computed_path
                    break

                # get neighbors for current
                neighbors = get_neighbors(current)
                for neighbor in neighbors:
                    reachable = colliding(neighbor)

                    # if reachable:
                    #     explored_nodes[neighbor] = neighbor
                    #     if neighbor in collision_nodes.keys():
                    #         del collision_nodes[neighbor]
                        
                    # else:
                    #     collision_nodes[neighbor] = neighbor
                    #     if neighbor in  explored_nodes.keys():
                    #         del  explored_nodes[neighbor]

                    if reachable and neighbor not in closedSet:
                        if neighbor in f.keys():
                            if (f[neighbor],neighbor) in openSet:
                                # if neighbor node in open list, check if current path is
                                # better than the one previously found for this neighbor
                                # node.

                                if g[neighbor] > g[current] + heuristicE(current,neighbor):
                                    update_node(neighbor,current)
                        else:
                            update_node(neighbor, current)
                            # add neighbor node to open list
                            heapq.heappush(openSet,(f[neighbor],neighbor))
            if len(openSet) is None:
                print 'No Solution Found'
                

        x, y = robot.GetTransform()[0:2,3]
        aStar((-3.400,-1.400,0.00),goalconfig)
        
        #### Draw your path in the openrave here (see /usr/lib/python2.7/dist-packages/openravepy/_openravepy_0_8/examples/tutorial_plotting.py for examples)
        
        for x,y,z in computed_path :
        #computed trajectory x,y positions 
            handles.append(env.plot3(points=array(([x,y,0.05])), 
                                   pointsize=0.07,
                                   colors=array(((0,0,0))),
                                   drawstyle=1))

        
        #### Draw the X and Y components of the configurations explored by A*
        # n = 0
        # while n in explored_nodes:
        #     n = explored_nodes[n]
        #     handles.append(env.plot3(points=array(([n[0],n[1],0.05])), 
        #                            pointsize=0.05,
        #                            colors=array(((0,0,1))),
        #                            drawstyle=1))

        # while n in collision_nodes:
        #     n = collision_nodes[n]
        #     handles.append(env.plot3(points=array(([n[0],n[1],0.05])), 
        #                            pointsize=0.05,
        #                            colors=array(((1,0,0))),
        #                            drawstyle=1))

        # for x,y,z in explored_nodes :
        # #computed trajectory x,y positions 
        #         handles.append(env.plot3(points=array(([x,y,0.05])), 
        #                                pointsize=0.05,
        #                                colors=array(((0,0,1))),
        #                                drawstyle=1))

        # for x,y,z in collision_nodes :
        # #computed trajectory x,y positions 
        #         handles.append(env.plot3(points=array(([x,y,0.05])), 
        #                                pointsize=0.05,
        #                                colors=array(((1,0,0))),
        #                                drawstyle=1))

        #### Now that you have computed a path, execute it on the robot using the controller. You will need to convert it into an openrave trajectory. You can set any reasonable timing for the configurations in the path. Then, execute the trajectory using robot.GetController().SetPath(mypath);

        traj = RaveCreateTrajectory(env,'')
        traj.Init(robot.GetActiveConfigurationSpecification())
        i=0
        for trajectories in computed_path:
            traj.Insert(i,trajectories)
            i = i+1

        planningutils.RetimeActiveDOFTrajectory(traj,robot,hastimestamps=False,maxvelmult=1)
        print 'duration',traj.GetDuration()
    robot.GetController().SetPath(traj)
    robot.WaitForController(0)


        #### END OF YOUR CODE ###
    waitrobot(robot)

    raw_input("Press enter to exit...")

