#!/usr/bin/env python
# -*- coding: utf-8 -*-
#HW1 for RBE 595/CS 525 Motion Planning
#code based on the simplemanipulation.py example
#Author : Varun Visnudas Verlencar
import time
import openravepy

if not __openravepy_build_doc__:
    from openravepy import *
    from numpy import *

def waitrobot(robot):
    """busy wait for robot completion"""
    while not robot.GetController().IsDone():
        time.sleep(0.01)

def tuckarms(env,robot):
    with env:
        jointnames = ['l_shoulder_lift_joint','l_elbow_flex_joint','l_wrist_flex_joint','r_shoulder_lift_joint','r_elbow_flex_joint','r_wrist_flex_joint']
        robot.SetActiveDOFs([robot.GetJoint(name).GetDOFIndex() for name in jointnames])
        robot.SetActiveDOFValues([1.29023451,-2.32099996,-0.69800004,1.27843491,-2.32100002,-0.69799996]);
        robot.GetController().SetDesired(robot.GetDOFValues());
    waitrobot(robot)

if __name__ == "__main__":

    env = Environment()
    env.SetViewer('qtcoin')
    env.Reset()
    # load a scene from ProjectRoom environment XML file
    env.Load('data/pr2test2.env.xml')
    time.sleep(0.1)

    # 1) get the 1st robot that is inside the loaded scene
    # 2) assign it to the variable named 'robot'
    robot = env.GetRobots()[0]

    # tuck in the PR2's arms for driving
    tuckarms(env,robot);


    #### YOUR CODE HERE ####

    with env:
        #Table1
        env.GetKinBody('Table1').SetTransform(matrixFromPose([0.70711,0.0,0.0,0.70711,-1.66683,-1.38455,0.74001])) # quaternion + translation to 4x4 matix and set new pose

        #Table2
        env.GetKinBody('Table2').SetTransform(matrixFromPose([0.70711,0.0,0.0,0.70711,-2.15626,1.24453,0.74000])) # quaternion + translation to 4x4 matix and set new pose

        #Table3
        env.GetKinBody('Table3').SetTransform(matrixFromPose([0.70711,0.0,0.0,0.70711,-1.52935,0.39124,0.74001])) # quaternion + translation to 4x4 matix and set new pose

        #Table4
        env.GetKinBody('Table4').SetTransform(matrixFromPose([0.70711,0.0,0.0,0.70711,-0.92357,1.24050,0.74001])) # quaternion + translation to 4x4 matix and set new pose

        #Table4
        env.GetKinBody('Table5').SetTransform(matrixFromPose([0.95550,0.0,0.0,-0.29501,0.05664,-1.16921,0.74000])) # quaternion + translation to 4x4 matix and set new pose

        #Table6
        env.GetKinBody('Table6').SetTransform(matrixFromPose([1.0,0.0,0.0,0.0,-3.27133,0.97127,0.73999])) # quaternion + translation to 4x4 matix and set new pose

        #TibitsBox1
        env.GetKinBody('TibitsBox1').SetTransform(matrixFromPose([1.0,0.0,0.0,0.0,0.06083,-0.89023,0.7400])) # quaternion + translation to 4x4 matix and set new pose

    # env.UpdatePublishedBodies() # allow viewer to update new robot

    #### END OF YOUR CODE ###


    raw_input("Press enter to exit...")

