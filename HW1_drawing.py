#!/usr/bin/env python
# -*- coding: utf-8 -*-
#HW1 for RBE 595/CS 525 Motion Planning
#code based on the simplemanipulation.py example
#Author : Varun Visnudas Verlencar
import time
import openravepy
import numpy as np
if not __openravepy_build_doc__:
    from openravepy import *
    from numpy import *

def waitrobot(robot):
    """busy wait for robot completion"""
    while not robot.GetController().IsDone():
        time.sleep(0.01)

def tuckarms(env,robot):
    with env:
        jointnames = ['l_shoulder_lift_joint','l_elbow_flex_joint','l_wrist_flex_joint','r_shoulder_lift_joint','r_elbow_flex_joint','r_wrist_flex_joint']
        robot.SetActiveDOFs([robot.GetJoint(name).GetDOFIndex() for name in jointnames])
        robot.SetActiveDOFValues([1.29023451,-2.32099996,-0.69800004,1.27843491,-2.32100002,-0.69799996]);
        robot.GetController().SetDesired(robot.GetDOFValues());
    waitrobot(robot)

if __name__ == "__main__":

    env = Environment()
    env.SetViewer('qtcoin')
    env.Reset()
    # load a scene from ProjectRoom environment XML file
    env.Load('data/pr2test2.env.xml')
    time.sleep(0.1)

    # 1) get the 1st robot that is inside the loaded scene
    # 2) assign it to the variable named 'robot'
    robot = env.GetRobots()[0]

    # tuck in the PR2's arms for driving
    tuckarms(env,robot);


    #### YOUR CODE HERE ####
    
    table_name = ['Table1','Table2','Table3','Table4','Table5','Table6']
    handles = [] 

    with env:
        for name in table_name:
            table = env.GetKinBody(name)    #table referenced by name
            table_dim = table.ComputeAABB().extents() # get dimensions
            pos = table.ComputeAABB().pos() #get coordinate frame
            
            #coordinates of cuboid
            #bottom vertices
            bottom_left_down = pos + [-table_dim[0],-table_dim[1],-pos[2]]
            bottom_left_up = pos + [-table_dim[0],table_dim[1],-pos[2]]
            bottom_right_down = pos + [table_dim[0],-table_dim[1],-pos[2]]
            bottom_right_up = pos + [table_dim[0],table_dim[1],-pos[2]]

            #top vertices
            top_left_down = pos + [-table_dim[0],-table_dim[1],pos[2]]
            top_left_up = pos + [-table_dim[0],table_dim[1],pos[2]]
            top_right_down = pos + [table_dim[0],-table_dim[1],pos[2]]
            top_right_up = pos + [table_dim[0],table_dim[1],pos[2]]

            #draw the cuboid
            handles.append(env.drawlinestrip(points=array((bottom_left_up,bottom_left_down,bottom_right_down,bottom_right_up,bottom_left_up,
                                                           top_left_up,top_left_down,top_right_down,top_right_up,top_left_up,
                                                           top_left_down,bottom_left_down,bottom_right_down,top_right_down,top_right_up,
                                                           bottom_right_up)),
                                               linewidth=3.0,
                                               colors=array(((1,0,0)))))

        envr = env.GetKinBody('ProjectRoom')
        envr_dim = envr.ComputeAABB().extents()
        envr_pos = envr.ComputeAABB().pos()
       
        envr_dim_transpose = matrixFromPose([1.0,0.0,0.0,0.0,envr_dim[0],envr_dim[1],envr_dim[2]]) # convert to 4x4
        
        for i in range(0,35):
            quat = quatFromAxisAngle([0.0,0.0,(3.14/18.0)*i])   #rotate by 10 degree
            new_pos = numpy.dot(matrixFromQuat(quat),envr_dim_transpose)    #transform
            handles.append(env.plot3(points=array((new_pos[0:3,3])),    #get x,y,z positions and pot points
                                       pointsize=0.15,
                                       colors=array(((0,0,1))),
                                       drawstyle=1))



    #### END OF YOUR CODE ###


    raw_input("Press enter to exit...")

